# README #

There's not much to this one. The only other file in this repository (besides this README, of course) is a Postman collection. That collection has exactly two requests included in it:

1. The first one is a request for a Drop-In Module (DIM) token using an external user id (extUserId). That id can be whatever unique identifier that you use in your application that you associate with the user you are attempting to load the DIM for.
2. The second one is a request for a Drop-In Module (DIM) token using an Spinwheel user id (userId). That id is provided to you once a user has logged in using the Connect DIM.

## Using This Repository ##

Simply clone this repository and open the Postman collection inside of Postman. All the relevant values are set up as collection variables, so simply replace the values that the variables are set to with your own values (your secretKey, an external user id for a user, and a Spinwheel UserId once you have one).